// Generate Buttons
const numBtnsNum = 10;

let btn;
let output;

// Operater list
const listOperators = ["AC","DEL","(",")","÷","x","+","-","^","√",".","="];

// Generate Layout section
var calcLayout = document.createElement("section");
    calcLayout.setAttribute("class", "calcLayout");
    calcLayout.setAttribute("id", "calcLayout-id");
    document.body.appendChild(calcLayout);

// Generate output field section
var outputSection = document.createElement("section");
    outputSection.setAttribute("class", "output");
    outputSection.setAttribute("id", "output-field");
    var layoutSection = document.getElementById("calcLayout-id");
    layoutSection.appendChild(outputSection);

// Populate the output section
for (let i = 0; i < 2; i++) {
    output = document.createElement("output");
    output.className = "output output-" + [i];
    output.id = "output";
    var btnSec = document.getElementById("output-field");
    //  used to append nodes (typically elements) at the end of a specified parent node.
    btnSec.appendChild(output);
}

// Populate the num buttons
for (let i = 0; i < numBtnsNum; i++) {
    btn = document.createElement("button");
    btn.innerHTML = i;
    btn.className = "btn";
    btn.id = "num-btn";
    var btnSec = document.getElementById("calcLayout-id");
    //  used to append nodes (typically elements) at the end of a specified parent node.
    btnSec.appendChild(btn);
}

// Populate the symbols section
for (let i = 0; i < listOperators.length; i++) {
    btn = document.createElement("button");
    btn.innerHTML = listOperators[i];
    btn.className = "btn";

    switch(listOperators[i]){
    case "=":
        btn.id = "equals-btn";
        break;
    case "AC":
        btn.id = "ac-btn";
        break;
    case "DEL":
        btn.id = "del-btn";
        break;
    default:
        btn.id = "num-btn";
        break;
    }
    var btnSec = document.getElementById("calcLayout-id");
    btnSec.appendChild(btn);
}


class Calculator{
    constructor(firstOutputVal,lastOutputVal){
        this.firstOutputVal = firstOutputVal;
        this.lastOutputVal = lastOutputVal;
        this.Clear();
    }

    Clear(){
        this.currentInput = "";
        this.previousInput = "";
        this.operation = undefined;
    }

    DeleteNum(){
        this.currentInput = this.currentInput.toString().slice(0, -1);
    }

    AppendNum(number){
        this.currentInput = this.currentInput.toString() + number.toString();
    }

    SetEquation()
    {   
        this.Calculate(this.currentInput);
    }

    Calculate(equationToCalc){        
        var brackets = [];
        var operators = [];
        var operands = [];
        var operandsString = "";
        

        for (let i = 0, operatorIndex = 0, bracketIndex = 0; i < equationToCalc.length; i++) {
            if(equationToCalc.charAt(i) == "+"
            || equationToCalc.charAt(i) == "-"
            || equationToCalc.charAt(i) == "x"
            || equationToCalc.charAt(i) == "÷"
            || equationToCalc.charAt(i) == "√"
            || equationToCalc.charAt(i) == "^")
            {
                // Handles if there are two pairs of brackets being check against each other for example (1+2) + (3 + 4)
                if(equationToCalc.charAt(i - 1) == ")" && equationToCalc.charAt(i + 1) == "(")
                {
                    operators[operatorIndex++] = "x";
                    brackets[bracketIndex++] = null;
                    brackets[bracketIndex++] = null;
                    operators[operatorIndex++] = equationToCalc.charAt(i);
                    operandsString += " 1 ";
                    
                }
                // Fetching operators
                else{
                    operators[operatorIndex++] = equationToCalc.charAt(i);
                    brackets[bracketIndex++] = null;

                    operandsString += " ";
                    
                }
            }

            else if(equationToCalc.charAt(i) == "("
            || equationToCalc.charAt(i) == ")")
            {
                brackets[bracketIndex++] = equationToCalc.charAt(i);
                operators[operatorIndex++] = null;
            }
            else{
                operandsString += equationToCalc.charAt(i);
            }
        }
        var operandsTemp = operandsString.split(" ");
        

        for (var i = 0, indexOperand = 0; i < operators.length; i++) {
            if(brackets[i] == null)
            {
                operands[i] = Number(operandsTemp[indexOperand++]);
            }
            else{
                operands[i] = null;
            }
        }
        operands[operands.length] = Number(operandsTemp[indexOperand]);
        

        // Result variables
        this.result = 0;
        this.finalResult = 0;

        // Are the brackets?
        if(brackets.length > 0)
        {
            
                for (var i = 0; i < brackets.length; i++) {
                    
                    // checking for the closing bracket and
                    // Associating that with an opening bracket
                    if(brackets[i] == ")")
                    {
                        var indexBracketEnd = i;
                        for (var indexBracket = indexBracketEnd + 1; indexBracket >= 0; indexBracket--) {
                            if(brackets[indexBracket] == "(")
                            {
                                var indexBracketStart = indexBracket;
                                this.evaluate(indexBracketStart, indexBracketEnd, operators, operands);
                                brackets[indexBracketStart] = null;
                                brackets[indexBracketEnd] = null;
                            }
                        }
                    }
                }
        }

        
        this.finalResult = this.evaluate(0, operators.length, operators,operands);

        this.currentInput = this.finalResult;
        this.UpdateOutput();
    }

    evaluate(indexStartPos, indexEndPos, operators, operands)
    {

        // Roots and exponents
        for (var i = indexStartPos; i < indexEndPos; i++) {
            var indexOperandLeft = i;
            var indexOperandRight = i + 1;
            var indexNextOperator = i;

            
            
            if(operators[indexNextOperator] == null)
            {
                for (var iNext = indexNextOperator + 1; iNext < operators.length; iNext++) {
                    if(operators[iNext] != null)
                    {
                        indexNextOperator = iNext;
                        indexOperandLeft = iNext;
                        indexOperandRight = iNext + 1;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            if(operands[indexOperandRight] == null)
            {
                for (var iNext = indexOperandRight + 1; iNext < operands.length; iNext++) {
                    if(operands[iNext] != null)
                    {
                        indexOperandRight = iNext;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            // Multiplication
            if(operators[indexNextOperator] == "√")
            {
                this.result = operands[indexOperandRight] ** (1/operands[indexOperandLeft]);

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }

            else if(operators[indexNextOperator] == "^")
            {
                this.result = operands[indexOperandLeft] ** operands[indexOperandRight];

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }
        }

        // First evaluate multiplication and division
        for (var i = indexStartPos; i < indexEndPos; i++) {
            var indexOperandLeft = i;
            var indexOperandRight = i + 1;
            var indexNextOperator = i;

            
            
            if(operators[indexNextOperator] == null)
            {
                for (var iNext = indexNextOperator + 1; iNext < operators.length; iNext++) {
                    if(operators[iNext] != null)
                    {
                        indexNextOperator = iNext;
                        indexOperandLeft = iNext;
                        indexOperandRight = iNext + 1;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            if(operands[indexOperandRight] == null)
            {
                for (var iNext = indexOperandRight + 1; iNext < operands.length; iNext++) {
                    if(operands[iNext] != null)
                    {
                        indexOperandRight = iNext;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            // Multiplication
            if(operators[indexNextOperator] == "x")
            {
                this.result = operands[indexOperandLeft] * operands[indexOperandRight];

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }

            else if(operators[indexNextOperator] == "÷")
            {
                if(operands[indexOperandRight] == 0)
                {
                    return this.finalResult = "undefined";
                }
                this.result = operands[indexOperandLeft] / operands[indexOperandRight];

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }
        }

        // Addition and subtraction
        for (let i = indexStartPos; i < indexEndPos; i++) {
            var indexOperandLeft = i;
            var indexOperandRight = i +1;
            var indexNextOperator = i;
            
            // null check
            if(operators[indexNextOperator] == null)
            {
                for (let iNext = indexNextOperator + 1; iNext < operators.length; iNext++) {
                    if(operators[iNext] != null)
                    {
                        indexNextOperator = iNext;
                        indexOperandLeft = iNext;
                        indexOperandRight = iNext + 1;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            if(operands[indexOperandRight] == null)
            {
                for (let iNext = indexOperandRight + 1; iNext < operands.length; iNext++) {
                    if(operands[iNext] != null)
                    {
                        indexOperandRight = iNext;
                        i = iNext - 1;
                        break;
                    }
                }
            }

            // Addition
            if(operators[indexNextOperator] == "+")
            {
                this.result = operands[indexOperandLeft] + operands[indexOperandRight];

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }
            // Subtraction
            else if(operators[indexNextOperator] == "-")
            {
                this.result = operands[indexOperandLeft] - operands[indexOperandRight];

                operands[indexOperandRight] = this.result;
                operands[indexOperandLeft] = null;
                operators[indexNextOperator] = null;
                this.finalResult = this.result;
                this.result = 0;
            }
        }

        // Return the final result
        return this.finalResult;
    } 

    UpdateOutput(){
        this.firstOutputVal.innerText = this.currentInput;
        if(this.operation != null){
            this.lastOutputVal.innerText = 
            `${this.previousInput} ${this.operation}`;
        }
        else{
            this.lastOutputVal.innerText = "";
        }
    }
}
const numbers = document.querySelectorAll("[id^='num-btn']");
const operators = document.querySelectorAll("[id^='symbol-btn']");
const outputArea = document.querySelectorAll("[id^='output']");
const equalsButton = document.querySelector("[id^='equals-btn']");
const clearAllButton = document.querySelector("[id^='ac-btn']");
const deleteNumButton = document.querySelector("[id^='del-btn']");

var previousInputNum = outputArea[1];
var currentInputNum = outputArea[2];



const calculator = new Calculator(currentInputNum, previousInputNum);

numbers.forEach(button =>{
    button.addEventListener('click', () =>{
        if(calculator.currentInput == "undefined")
        {
            calculator.Clear();
        }
        calculator.AppendNum(button.innerText);
        calculator.UpdateOutput();
    })
})

operators.forEach(button =>{
    button.addEventListener('click', () =>{
        calculator.ChooseOperation(currentInputEquation);
        calculator.UpdateOutput();
    })
})

equalsButton.addEventListener('click', button => {
    calculator.SetEquation();
    calculator.UpdateOutput();
})

clearAllButton.addEventListener('click', button => {
    calculator.Clear();
    calculator.UpdateOutput();
})

deleteNumButton.addEventListener('click', button => {
    calculator.DeleteNum();
    calculator.UpdateOutput();
})

